package com.hoanghung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value="com.hoanghung")
public class XemLichBongDaApplication {
	public static void main(String[] args) {
		SpringApplication.run(XemLichBongDaApplication.class, args);
	}
}
