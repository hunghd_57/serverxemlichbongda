package com.hoanghung.controller;

import common.HibernateUtils;
import model.MatchEntity;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sun.rmi.runtime.Log;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by HoangHung on 3/4/2016.
 */
@RestController
public class MatchController {
    @RequestMapping(value= "/match" )
    public List<MatchEntity> getMatch(){
        SessionFactory sessionFactory= HibernateUtils.getSessionFactory();
        Session session=sessionFactory.getCurrentSession();
        session.beginTransaction();
        Criteria criteria=session.createCriteria(MatchEntity.class);
        List<MatchEntity> list=  criteria.list();
        return list;
    }
    @RequestMapping(value = "/createMatch")
    public void createMatch(@RequestParam("idMatch") int id,@RequestParam("teamA") String teamA,@RequestParam("teamB") String teamB){
        MatchEntity matchEntity=new MatchEntity(id,teamA,teamB,"5/3/2016");
        SessionFactory sessionFactory=HibernateUtils.getSessionFactory();
        Session session=sessionFactory.getCurrentSession();
        session.beginTransaction();
        session.persist(matchEntity);
        session.getTransaction().commit();
    }

    @RequestMapping(value = "/updateMatch")
    public void updateMatch(@RequestParam("idMatch") int id,@RequestParam("teamA") String teamA,@RequestParam("teamB") String teamB){
        SessionFactory sessionFactory=HibernateUtils.getSessionFactory();
        Session session=sessionFactory.getCurrentSession();
        session.beginTransaction();
        MatchEntity matchEntity= MatchEntity.findMatchById(session,id);
        matchEntity.setNameClubA(teamA);
        matchEntity.setNameClubB(teamB);
        session.flush();
        session.getTransaction().commit();
    }

    @RequestMapping(value = "/deleteMatch")
    public void updateMatch(@RequestParam("idMatch") int id){
        SessionFactory sessionFactory=HibernateUtils.getSessionFactory();
        Session session=sessionFactory.getCurrentSession();
        session.beginTransaction();
        MatchEntity matchEntity= MatchEntity.findMatchById(session,id);
        session.delete(matchEntity);
        session.getTransaction().commit();
    }

}
