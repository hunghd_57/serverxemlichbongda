package model;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import javax.persistence.*;

/**
 * Created by HoangHung on 3/4/2016.
 */
@Entity
@Table(name = "_match", schema = "xemlichbongda", catalog = "")
public class MatchEntity {
    private int idMatch;
    private String nameClubA;
    private String nameClubB;
    private String datMatch;

    public MatchEntity() {

    }
    public MatchEntity(int idMatch, String A, String B, String date) {
        this.idMatch = idMatch;
        this.nameClubA = A;
        this.nameClubB = B;
        this.datMatch = date;
    }

    public static MatchEntity findMatchById(Session session,int idMatch){
        Criteria criteria=session.createCriteria(MatchEntity.class);
        criteria.add(Restrictions.eq("idMatch",idMatch));
        return (MatchEntity) criteria.uniqueResult();
    }
    @Id
    @Basic
    @Column(name = "idMatch")
    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    @Basic
    @Column(name = "nameClubA")
    public String getNameClubA() {
        return nameClubA;
    }

    public void setNameClubA(String nameClubA) {
        this.nameClubA = nameClubA;
    }

    @Basic
    @Column(name = "nameClubB")
    public String getNameClubB() {
        return nameClubB;
    }

    public void setNameClubB(String nameClubB) {
        this.nameClubB = nameClubB;
    }

    @Basic
    @Column(name = "datMatch")
    public String getDatMatch() {
        return datMatch;
    }

    public void setDatMatch(String datMatch) {
        this.datMatch = datMatch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MatchEntity that = (MatchEntity) o;

        if (idMatch != that.idMatch) return false;
        if (nameClubA != null ? !nameClubA.equals(that.nameClubA) : that.nameClubA != null) return false;
        if (nameClubB != null ? !nameClubB.equals(that.nameClubB) : that.nameClubB != null) return false;
        if (datMatch != null ? !datMatch.equals(that.datMatch) : that.datMatch != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idMatch;
        result = 31 * result + (nameClubA != null ? nameClubA.hashCode() : 0);
        result = 31 * result + (nameClubB != null ? nameClubB.hashCode() : 0);
        result = 31 * result + (datMatch != null ? datMatch.hashCode() : 0);
        return result;
    }
}
